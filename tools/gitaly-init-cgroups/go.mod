module gitlab.com/gitlab-org/gitaly/tools/gitaly-init-cgroups

go 1.23

toolchain go1.23.6

require (
	github.com/containerd/cgroups/v3 v3.0.5
	github.com/stretchr/testify v1.10.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/moby/sys/userns v0.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.27.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
